package ru.dolbak.customlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        ArrayList<HashMap<String, Object>> arrayList = new ArrayList<>();
        HashMap<String, Object> map;

        for (int i = 0; i < 10; i++){
            map = new HashMap<>();
            map.put("title", "15.6\"Ультрабук HUAWEI MateBook D 15 BoM-WFQ9 серебристый");
            map.put("description", "Full HD (1920x1080), IPS, AMD Ryzen 5 5500U, ядра: 6 х 2.1 ГГц, RAM 16 ГБ, SSD 512 ГБ, AMD Radeon Graphics , Windows 11 Home Single Language");
            map.put("price", "59 999 ₽");
            map.put("image", R.drawable.laptop1);
            arrayList.add(map);
        }

        SimpleAdapter adapter = new SimpleAdapter(this,
                arrayList,
                R.layout.list_view_item_layout,
                new String[]{"title", "description", "price", "image"},
                new int[]{R.id.title, R.id.description, R.id.price, R.id.imageView});

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                arrayList.remove(position);
                adapter.notifyDataSetInvalidated();
            }
        });
    }
}